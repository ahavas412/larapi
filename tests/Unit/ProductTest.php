<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProductTest extends TestCase
{
    private $baseHeader = [
        'Content-Type' => 'application/json',
    ];

    public function testCreate()
    {
        $response = $this->withHeaders($this->baseHeader)
            ->json('POST', '/api/products', [
                'stock' => 25,
                'price' => 123.5,
                'category_id' => 1,
                'name' => 'TestName',
                'description' => 'TestDescription Product',
            ]);
        $response->assertStatus(201);
    }
    
    public function testList()
    {
        $response = $this->withHeaders($this->baseHeader)
            ->json('GET', '/api/products', ['includeTrashed' => true]);
        $response->assertStatus(200);
    }

    public function testUpdate()
    {
        $response = $this->withHeaders($this->baseHeader)
            ->json('PUT', '/api/products/1', [
                'stock' => 30,
                'price' => 126.5,
                'category_id' => 1,
                'name' => 'TestUpdatedName',
                'description' => 'TestDescription Product Updated',
            ]);
        $response->assertStatus(200);
    }

    public function testSoftDelete()
    {
        $response = $this->withHeaders($this->baseHeader)
            ->json('DELETE', '/api/products/1');
        $response->assertStatus(204);
    }

    public function testRestore()
    {
        $response = $this->withHeaders($this->baseHeader)
            ->json('POST', '/api/products/1/restore');
        $response->assertStatus(200);
    }
}
