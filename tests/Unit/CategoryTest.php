<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CategoryTest extends TestCase
{
    private $baseHeader = [
        'Content-Type' => 'application/json',
    ];

    public function testCreate()
    {
        $response = $this->withHeaders($this->baseHeader)
            ->json('POST', '/api/categories', [
                'name' => 'TestName',
            ]);
        $response->assertStatus(201);
    }

    public function testList()
    {
        $response = $this->withHeaders($this->baseHeader)
            ->json('GET', '/api/categories', ['includeTrashed' => true]);
        $response->assertStatus(200);
    }

    public function testUpdate()
    {
        $response = $this->withHeaders($this->baseHeader)
            ->json('PUT', '/api/categories/1', ['name' => 'testUpdatedName']);
        $response->assertStatus(200);
    }

    public function testSoftDelete()
    {
        $response = $this->withHeaders($this->baseHeader)
            ->json('DELETE', '/api/categories/1');
        $response->assertStatus(204);
    }

    public function testRestore()
    {
        $response = $this->withHeaders($this->baseHeader)
            ->json('POST', '/api/categories/1/restore');
        $response->assertStatus(200);
    }
}
