<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    protected $table = 'products';

    protected $primaryKey = 'id';

    protected $fillable = ['name', 'category_id', 'description', 'price', 'stock'];

    protected $hidden = [];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
