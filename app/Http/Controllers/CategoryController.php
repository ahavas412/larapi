<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function index(Request $request)
    {
        $datas = $request->validate([
            'includeTrashed' => 'boolean',
        ]);
        $categories = (isset($datas['includeTrashed']) && $datas['includeTrashed']) ? Category::withTrashed()->get() : Category::all();

        return new JsonResponse($categories, Response::HTTP_OK);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function create()
    {
        return new JsonResponse([
            'code' => 405,
            'message' => 'Invalid Request',
        ], Response::HTTP_METHOD_NOT_ALLOWED);
    }

    /**
     * Create a new category with method POST
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $category = Category::create([
            'name' => $request->request->get('name'),
        ]);

        return new JsonResponse($category, Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function show(Request $request, $id)
    {
        $datas = $request->validate([
            'isTrashed' => 'boolean',
        ]);
        if (isset($datas['isTrashed']) && $datas['isTrashed']) {
            if (Category::withTrashed()->where('id', $id)->get() === null) return new JsonResponse(null, Response::HTTP_NOT_FOUND);
            return new JsonResponse (Category::withTrashed()->where('id', $id)->get(), Response::HTTP_OK);
        }
        if ($category = Category::where('id', $id)->first() === null) {
            return new JsonResponse([
                'code' => 404,
                'message' => 'No Category found with id '.$id,
            ], Response::HTTP_NOT_FOUND);
        }

        return new JsonResponse(Category::where('id', $id)->first(), Response::HTTP_OK);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function edit($id)
    {
        return new JsonResponse([
            'code' => 405,
            'message' => 'Invalid Request',
        ], Response::HTTP_METHOD_NOT_ALLOWED);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $data = $request->validate([
            'name' => 'string|required'
        ]);
        $category = Category::findOrFail($id);
        $category->name = $data['name'];
        $category->save();
        
        return new JsonResponse($category, Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function destroy($id)
    {
        Category::findOrFail($id)->delete();

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * Restore a deleted Category
     *
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function restore($id)
    {
        $category = Category::withTrashed($id);
        $category->restore();

        return new JsonResponse($category, Response::HTTP_OK);
    }
}
