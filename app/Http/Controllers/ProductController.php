<?php

namespace App\Http\Controllers;

use App\Product;
use App\Category;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function index(Request $request)
    {
        $datas = $request->validate([
            'includeTrashed' => 'boolean',
        ]);
        $products = (isset($datas['includeTrashed']) && $datas['includeTrashed']) ? Product::withTrashed()->get() : Product::all();

        return new JsonResponse($products, Response::HTTP_OK);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function create()
    {
        return new JsonResponse([
            'code' => 405,
            'message' => 'Invalid Request',
        ], Response::HTTP_METHOD_NOT_ALLOWED);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function store(Request $request)
    {
        $datas = $request->validate([
            'name' => 'string|required',
            'category_id' => 'required',
            'description' => 'string|required',
            'price' => 'required',
            'stock' => 'int|required',
        ]);
        if (Category::find($datas['category_id']) === null) {
            return new JsonResponse([
                'code' => 400,
                'message' => 'No Category found with the ID '.$datas['category_id'],
            ], Response::HTTP_BAD_REQUEST);
        }
        $product = Product::create([
            'name' => $datas['name'],
            'category_id' => $datas['category_id'],
            'description' => $datas['description'],
            'price' => $datas['price'],
            'stock' => $datas['stock'],
        ]);

        return new JsonResponse($product, Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function show($id)
    {
        $datas = $request->validate([
            'isTrashed' => 'boolean',
        ]);
        if (isset($datas['isTrashed']) && $datas['isTrashed']) {
            if (Product::withTrashed()->where('id', $id)->get() === null) return new JsonResponse(null, Response::HTTP_NOT_FOUND);
            return new JsonResponse (Product::withTrashed()->where('id', $id)->get(), Response::HTTP_OK);
        }
        if (Product::where('id', $id)->first() === null) {
            return new JsonResponse([
                'code' => 404,
                'message' => 'No Category found with id '.$id,
            ], Response::HTTP_NOT_FOUND);
        }

        return new JsonResponse(Product::where('id', $id)->first(), Response::HTTP_OK);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function edit($id)
    {
        return new JsonResponse([
            'code' => 405,
            'message' => 'Invalid Request',
        ], Response::HTTP_METHOD_NOT_ALLOWED);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $datas = $request->validate([
            'name' => 'string|required',
            'category_id' => 'int|required',
            'description' => 'string|required',
            'price' => 'required',
            'stock' => 'integer|required',
        ]);
        $product = Product::findOrFail($id);
        if (Category::find($datas['category_id']) === null) {
            return new JsonResponse([
                'code' => 400,
                'message' => 'No Category found with the ID '.$datas['category_id'],
            ], Response::HTTP_BAD_REQUEST);
        }
        $product->name = $datas['name'];
        $product->category_id = $datas['category_id'];
        $product->description = $datas['description'];
        $product->price = $datas['price'];
        $product->stock = $datas['stock'];
        $product->save();
        
        return new JsonResponse($product, Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function destroy($id)
    {
        Category::findOrFail($id)->delete();

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * Restore a deleted Product
     *
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function restore($id)
    {
        $product = Product::withTrashed($id);
        $product->restore();

        return new JsonResponse($product, Response::HTTP_OK);
    }
}
